function buildTranslations(message, originLanguage, destLanguages){
    return Promise.try(function(){
      translations = {}; 
      destLanguages.map(function(currentLang){
        translations[currentLang.code] = {}
        db.model('Translation').translate(message,originLanguage.code,currentLang.code).then(function(translation){
          console.log('build Trans function'+translation);
          translations[currentLang.code] = translation;
        }); 
      }); 
      return translations;
    }); 
}